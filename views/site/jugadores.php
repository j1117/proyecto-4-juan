<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\widgets\ListView;
?>


<div class="site-index">
    <div class="row">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        #'itemView' => '_jugadores',
        #'options'=>['class'=>'col-sm-6 col-md-4'],
        #'itemView'=>['class'=>'card tarjeta']
        'viewParams'=> [
            
        ],
        'itemView'=> function($model){

    ?>
    
        <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <p>
                            <?php echo Html::img(\Yii::getAlias('@web').'/images/jugadores/'.$model->dorsal.'.jpg',['width'=>'120px','height'=>'120px']); ?>
                        </p>
                        <p>
                            Nombre: <?= $model->nombre?>
                        </p>
                    </div>
                </div>
           
        </div>
    <?php 
        }
        ]);
    ?>
   </div>              
</div>
