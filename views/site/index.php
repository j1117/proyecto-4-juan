<?php

/* @var $this yii\web\View */

$this->title = 'Ciclistas';

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\widgets\ListView;
?>
    <?php 
    $this->registerJs("$('#recipeCarousel').carousel({
        interval: 9000000
        })"
        ,View::POS_READY);
        
    $this->registerJs(" $('.carousel .carousel-item').each(function(){
            var minPerSlide = 1;
            var next = $(this).next();
            if (!next.length) {
            next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            for (var i=0;i<minPerSlide;i++) {
                next=next.next();
                if (!next.length) {
                        next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
              }
        });",
          View::POS_READY);
    
    $this->registerCss("@media (max-width: 768px) {
            .carousel-inner .carousel-item > div {
                display: none;
            }
            .carousel-inner .carousel-item > div:first-child {
                display: block;
            }
        }

        .carousel-inner .carousel-item.active,
        .carousel-inner .carousel-item-next,
        .carousel-inner .carousel-item-prev {
            display: flex;
        }

        /* display 3 */
        @media (min-width: 768px) {

            .carousel-inner .carousel-item-right.active,
            .carousel-inner .carousel-item-next {
              transform: translateX(33.333%);
            }

            .carousel-inner .carousel-item-left.active, 
            .carousel-inner .carousel-item-prev {
              transform: translateX(-33.333%);
            }
        }

        .carousel-inner .carousel-item-right,
        .carousel-inner .carousel-item-left{ 
          transform: translateX(0);
        }"
            );
    ?>
    
<div class="site-index">
    <div class="col-lg-10 col-md-12">
        <div class="">
            <p>Ciclista favorito: </p>
            
            <?= Html::a('jugadores',['site/jugadores'],['class'=>'btn btn-primary']) ?>
        </div>
        
       
    </div>
    
     <div class="row mx-auto my-auto">
        <div id="recipeCarousel" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner w-100" role="listbox">
                <div class="carousel-item active">
                    <div class="col-md-4">
                        <div class="card card-body">
                            <img class="img-fluid" src="http://placehold.it/380?text=1">
                        </div>
                    </div>
                </div>
                
    <!--OBJETOS PARA LOS EQUIPOS-->
        
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        #'itemView' => '_jugadores',
                        #'options'=>['class'=>'col-sm-6 col-md-4'],
                        #'itemView'=>['class'=>'card tarjeta']
                        #'layout'=>['summaryItems' ],
                        'itemView'=> function($model){

                    ?>
    
                <div class="carousel-item">
                    <div class="col-md-4">
                        <div class="card card-body">
                            <?= Html::a(Html::img('@web/images/equipos/'.$model['nomequipo'].'.jpg',['width'=>'100%','height'=>'300px']),
                                    ['site/jugadores','nombrequipo'=>$model['nomequipo']],
                                    ['class'=>'img-fluid']) ?>
                            <p id="nomequipo">
                                <?= $model['nomequipo'] ?>
                            </p>
                        </div>
                    </div>
                </div>
                   
                    <?php 
                        }
                        ]);
                    ?>
            <a class="carousel-control-prev w-auto" href="#recipeCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                <span class="sr-only">anterior</span>
            </a>
            <a class="carousel-control-next w-auto" href="#recipeCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                <span class="sr-only">siguiente</span>
            </a>
            </div>
        </div>
    </div>        
    <!--boton modal-->
    <!-- Button to Open the Modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  gestos
</button>
    
    <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modal Heading</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
</div>                
   

